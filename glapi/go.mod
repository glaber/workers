module glaber.io/glapi

go 1.20

require (
	github.com/valyala/bytebufferpool v1.0.0
	github.com/valyala/fastjson v1.6.4
	gopkg.in/mcuadros/go-syslog.v2 v2.3.0
)

require git.glaber.ru/extra/gosnmp v0.0.0-20240529060424-f283d1ce11cd // indirect

require (
	//	git.glaber.ru/extra/gosnmp v0.0.0-20240523061018-47ffca428d28 // indirect
	github.com/cdevr/WapSNMP v0.0.0-20160619095409-94cbc6d18750 // indirect
	github.com/ebookbug/gosnmptrap v0.0.0-20150925081200-01a706c0f994 // indirect
	github.com/gosnmp/gosnmp v1.38.0
	gopkg.in/check.v1 v0.0.0-20161208181325-20d25e280405 // indirect
)
