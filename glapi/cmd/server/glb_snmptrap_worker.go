package main

import (
        "encoding/json"
        "fmt"
        "net"
        "github.com/ebookbug/gosnmptrap"
        "log"
        "os"
)
 var strRemoteAddr = "st"

func main() {
        l := log.New(os.Stderr, "", 0)
        l.Printf("Start a new UDPServr\n")

        socket,err := net.ListenUDP("udp4",&net.UDPAddr{
                IP:net.IPv4(0,0,0,0),
                Port:162,
        })

        if err !=nil{
                panic(err)
        }

        defer socket.Close()

        for{
            buf := make([]byte,2048)
            read,from,_:=socket.ReadFromUDP(buf)
            strRemoteAddr = from.IP.String()
            go HandleUdp(buf[:read], strRemoteAddr)
        }
}

func HandleUdp(data []byte, strRemoteAddr string) (gosnmptrap.Trap) {
	trap,err := gosnmptrap.ParseUdp(data)

	if err !=nil{
    	fmt.Println("Err",err.Error())
  	}


	type ipaddr struct {
        Ip string
	}

	type combined struct {
        gosnmptrap.Trap
        ipaddr
	}

	s := combined{
    	trap,
    	ipaddr{strRemoteAddr},
	}

	by, err := json.Marshal(s)

	if err != nil {
    	fmt.Printf("Error: %s", err)
	}

	fmt.Println(string(by))

	return trap
}