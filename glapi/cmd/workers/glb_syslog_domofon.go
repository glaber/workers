package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"gopkg.in/mcuadros/go-syslog.v2"
)
/* 
 go get gopkg.in/mcuadros/go-syslog.v2
*/

/*
echo -n "<165>1 2003-10-11T22:14:15.003Z mymachine.example.com evntslog - \
    ID47 [exampleSDID@32473 iut=\"3\" eventSource=\"Application\" eventID=\"1011\"][examplePriority@32473 class=\"high\"]" | nc -4u -w1 127.0.0.1 10515
*/

func main() {
	var listenUDP string

	flag.StringVar(&listenUDP,"listen","0.0.0.0:514","UDP listen address")
	flag.Parse()
		
	channel := make(syslog.LogPartsChannel)
	
	handler := syslog.NewChannelHandler(channel)

	server := syslog.NewServer()
	server.SetFormat(syslog.Automatic)
	
	server.SetHandler(handler)
	server.ListenUDP(listenUDP)
	server.Boot()

	go process_messages(channel)

	server.Wait()
}

func process_messages(channel syslog.LogPartsChannel) {
	for logParts := range channel {
		
		str, err := json.Marshal(logParts)



		if (nil != err) {
			continue
		}
		
		fmt.Println(string(str))
	}
}


package internal

import (
	"fmt"
	"regexp"
	"strings"
	"syslog-eventer/internal/event"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/rs/zerolog/log"

	"gopkg.in/mcuadros/go-syslog.v2/format"
)

type Expr struct {
	Type     string
	Re       *regexp.Regexp
	Str      string
	TypeFunc func(data map[string]interface{}) string
	AddInfo  map[string]interface{}
	SubExpr  []Expr
}

type Parser struct {
	StateDb  StateDb
	ExprList []Expr
}

func CreateParser(stateDb StateDb) Parser {
	parser := Parser{
		StateDb: stateDb,
	}
	parser.initRegexpList()

	return parser
}

func (p *Parser) initRegexpList() {
	p.ExprList = []Expr{
		// INFO: Первые события очень популярны в логах, и лучше если они будут в начале цепочки проверки
		{
			Type: event.TypeUnknown,
			Str:  "Return D-Bus message from",
		},
		{
			// TODO: Это событие скорее всего нафиг не нужно никому
			Type: event.TypeRtspConnected,
			Re:   regexp.MustCompile(`New RTSP client \d+ (?P<ip>[\d.]+) connected`),
		},
		{
			// TODO: Это событие скорее всего нафиг не нужно никому
			Type: event.TypeGotImage,
			Re:   regexp.MustCompile(`Request: /cgi-bin/images_cgi\?channel=(?P<channel>\d+)&user=(?P<user>\w+)&pwd=\**, userLevel = (?P<userLevel>\d+)`),
		},
		{
			Type: event.TypeMotionDetectBegin,
			Str:  "SS_MAINAPI_ReportAlarmHappen",
		},
		{
			Type: event.TypeMotionDetectEnd,
			Str:  "SS_MAINAPI_ReportAlarmFinish",
		},
		{
			Type: event.TypeMotionDetectBegin,
			Str:  "EVENT: Detected motion in",
		},
		{
			Str: "Opening door by",
			SubExpr: []Expr{
				{
					// KLUDGE: Тут есть слово RFID которое используется в блоке ниже, а значит менять эти блоки местами следует осторожно
					Re: regexp.MustCompile(`Opening door by (?:(?P<rfid_type>[\w]+) )?RFID (?P<rfid_id>[0-9A-Z]+)(?:, apartment (?P<apartment>\d+))?`),
					TypeFunc: func(data map[string]interface{}) string {
						if data["rfid_type"] == "external" {
							return event.TypeOpenExternal
						} else {
							return event.TypeOpenInternal
						}
					},
				},
				{
					Type: event.TypeOpenHandset,
					Re:   regexp.MustCompile(`(?:\[(?P<pid>\d+)] )?Opening door by CMS handset for apartment (?P<apartment>\d+)`),
				},
				{
					Type: event.TypeSipOpen,
					Re:   regexp.MustCompile(`(?:\[(?P<pid>\d+)] )?Opening door by DTMF command for apartment (?P<apartment>\d+)`),
				},
				{
					Type: event.TypeOpenServiceCode,
					Re:   regexp.MustCompile(`Opening door by service door code (?P<code>\d+)?`),
				},
				{
					Type: event.TypeOpenCode,
					Re:   regexp.MustCompile(`Opening door by code (?P<code>\d+), apartment (?P<apartment>\d+)`),
				},
			},
		},
		{
			Str: "CMS handset",
			SubExpr: []Expr{
				{
					Re: regexp.MustCompile(`(?:\[(?P<pid>\d+)] )?CMS handset (?P<type>[\w]+) started for apartment (?P<apartment>\d+)`),
					TypeFunc: func(data map[string]interface{}) string {
						if data["type"] == "call" {
							return event.TypeHandsetCall
						} else {
							return event.TypeHandsetStart
						}
					},
				},
				{
					Type: event.TypeHandsetCallError,
					Re:   regexp.MustCompile(`(?:\[(?P<pid>\d+)] )?CMS handset is not connected for apartment (?P<apartment>\d+), aborting CMS call`),
				},
				{
					Type: event.TypeHandsetStop,
					Re:   regexp.MustCompile(`(?:\[(?P<pid>\d+)] )?CMS handset call done for apartment (?P<apartment>\d+), handset is down`),
				},
			},
		},
		{
			Str: "RFID",
			SubExpr: []Expr{
				{
					Type: event.TypeRfidNew,
					Re:   regexp.MustCompile(`RFID (?P<rfid_id>[0-9A-Z]+) is added by Auto Collect Mode`),
				},
				{
					Type: event.TypeRfidNew,
					Re:   regexp.MustCompile(`RFID key (?P<rfid_id>[0-9A-Fa-f]+) registered for apartment (?P<apartment>\d+)`),
					// TODO: преобразование rfid_id fmt.Sprintf("%014s", reMatch[i][2:])
				},
				{
					Type: event.TypeRfidFailed,
					Re:   regexp.MustCompile(`RFID (?P<rfid_id>[0-9A-Z]+) is not present in database`),
				},
				{
					Type: event.TypeRfidIncrementOk,
					Re:   regexp.MustCompile(`RFID (?P<rfid_id>[0-9A-Fa-f]+), apartment (?P<apartment>\d+), increment success:"(?P<old_inc>\d+)" => "(?P<new_inc>\d+)" \(sector: (?P<sector_num>\d+), block: (?P<block_num>\d+)\)`),
				},
				{
					Type:    event.TypeRfidIncrementFail,
					Re:      regexp.MustCompile(`RFID (?P<rfid_id>[0-9A-Fa-f]+), apartment (?P<apartment>\d+), increment failed: the key was removed early`),
					AddInfo: map[string]interface{}{"msg": "Increment failed: the key was removed early"},
				},
				{
					Type:    event.TypeRfidMifareMarkupFail,
					Re:      regexp.MustCompile(`RFID (?P<rfid_id>[0-9A-Fa-f]+), apartment (?P<apartment>\d+), mifare markup failed: authorization failed \(sector: (?P<sector_num>\d+)\)`),
					AddInfo: map[string]interface{}{"msg": "Mifare markup failed: authorization failed"},
				},
			},
		},
		{
			Str: "SIP",
			SubExpr: []Expr{
				{
					Type: event.TypeSipStop,
					Re:   regexp.MustCompile(`(?:\[(?P<pid>\d+)] )?SIP call done for apartment (?P<apartment>\d+)?, handset is down`),
				},
				{
					Type: event.TypeSipStart,
					Re:   regexp.MustCompile(`(?:\[(?P<pid>\d+)] )?SIP talk started for apartment (?P<apartment>\d+)?`),
				},
				{
					Type: event.TypeSipCall,
					Re:   regexp.MustCompile(`\[(?P<pid>\d+)] SIP call (?P<call_num>\d+) state changed to CALLING`),
				},
				{
					Type: event.TypeSipBreak,
					Re:   regexp.MustCompile(`Breaking SIP call (?P<call_num>\d+) because we got call (?P<got_call>\d+)`),
				},
				{
					Type: event.TypeSipExtBreaking,
					Re:   regexp.MustCompile(`(?:\[(?P<pid>\d+)] )?SIP: Breaking calls, external invoke...`),
				},
				{
					Type: event.TypeSipDtmf,
					Re:   regexp.MustCompile(`(?:\[(?P<pid>\d+)] )?SIP: Got DTMF command: (?P<command>\d+)`),
				},
			},
		},
		{
			Re: regexp.MustCompile(`Main door button (?P<press_type>[a-z]+)`),
			TypeFunc: func(data map[string]interface{}) string {
				if data["press_type"] == "unpressed" || data["press_type"] == "press" {
					return event.TypeBtnUp
				} else {
					return event.TypeBtnDown
				}
			},
		},
		{
			Type: event.TypeErrorRelay,
			Re:   regexp.MustCompile(`Error setting relay (?P<relay_num>[0-9A-Z()]+) to (?P<value>[\d]+)`),
		},
		{
			Type: event.TypeErrorAdc3,
			Str:  "Error getting ADC3 value",
		},
		{
			Type: event.TypeSosCall,
			Re:   regexp.MustCompile(`Calling for alarm \d+ started`),
		},
		{
			Type: event.TypeSosCall,
			Str:  "SOS_CALL_ESTABLISHED",
		},
		{
			Type: event.TypeConciergeCallEstablished,
			Str:  "CONCIERGE_CALL_ESTABLISHED",
		},
		{
			Type: event.TypeHandsetCall,
			Re:   regexp.MustCompile(`(?:\[(?P<pid>\d+)] )?Unable to call CMS apartment (?P<apartment>\d+), invalid line number -1!`),
		},
		{
			Type: event.TypeSipCall,
			Re:   regexp.MustCompile(`(?:\[(?P<pid>\d+)] )?Calling sip:(?P<sip_account>[\w@.:]+) through account`),
		},
		{
			Type: event.TypeCallsDone,
			Re:   regexp.MustCompile(`(?:\[(?P<pid>\d+)] )?All calls are done for apartment\s?(?P<apartment>\S+)`),
		},
		{
			Str: "by API command",
			SubExpr: []Expr{
				{
					Type: event.TypeOpenApi,
					Re:   regexp.MustCompile(`(?P<open_type>[\w\s]+) opening by API command`),
				},
				{
					Type: event.TypeOpenApi,
					Re:   regexp.MustCompile(`Opening (?P<open_type>[\w\s]+) by API command`),
				},
			},
		},
		{
			Type:    event.TypeRfidFailed,
			Re:      regexp.MustCompile(`Got invalid answer on command IP AUTHCL:(?P<rfid_id>[0-9A-Z]+):[0-9A-Z]+:[0-9A-Z]+`),
			AddInfo: map[string]interface{}{"type": "mifare|rfid"},
		},
		{
			Type:    event.TypeRfidFailed,
			Re:      regexp.MustCompile(`Unable to read code from key (?P<rfid_id>[0-9A-Z]+), invalid answer`),
			AddInfo: map[string]interface{}{"type": "mifare|rfid", "comment": "unable to read code from key"},
		},
		{
			Str: "Baresip",
			SubExpr: []Expr{
				{
					Type: event.TypeBaresip,
					Re:   regexp.MustCompile(`Baresip call started to flat (?P<apartment>\S+) with UUID = (?P<uuid>\S+)`),
				},
				{
					Type: event.TypeSipRegisterOk,
					Str:  "Baresip event REGISTER_OK",
				},
				{
					Type: event.TypeSipRegisterFail,
					Str:  "Baresip event REGISTER_FAIL",
				},
			},
		},
		{
			Type:    event.TypeBreakIn,
			Str:     "Intercom break in detected!!!",
			AddInfo: map[string]interface{}{"msg": "Intercom break in detected!!!"},
		},
		{
			Type: event.TypePowerOn,
			Str:  "DEVICE POWER ON!",
		},
		{
			Type: event.TypeSegfault,
			Str:  "Error Program Get SEGMENT Fault!",
		},
		{
			Type:    event.TypeBreakIn,
			Str:     "Main door break in detected!",
			AddInfo: map[string]interface{}{"msg": "Main door break in detected!"},
		},
		{
			Type:    event.TypeBreakIn,
			Str:     "Unable to send 'Ring_",
			AddInfo: map[string]interface{}{"msg": "Unable to send Ring command"},
		},
		{
			Type:    event.TypeBreakIn,
			Str:     "Unable to send sound 'Bi-Lim_",
			AddInfo: map[string]interface{}{"msg": "Unable to send sound Bi-Lim command"},
		},
		{
			Type:    event.TypeBreakIn,
			Str:     "Tamper alarm detected",
			AddInfo: map[string]interface{}{"msg": "Tamper alarm detected"},
		},
	}
}

func (p *Parser) ParseMessage(msg format.LogParts) []*event.Event {
	var ok bool
	ev := event.NewEvent()

	var msgStr string
	if msgStr, ok = msg["message"].(string); !ok {
		log.Debug().Msg("Parser: message not found in data")
		return []*event.Event{ev}
	}

	if t, ok := msg["timestamp"].(time.Time); ok {
		ev.SetTimestamp(t)
	} else {
		log.Debug().Msg("Parser: timestamp assert to time.Time error")
		return []*event.Event{ev}
	}

	if ev.DeviceId, ok = msg["proc_id"].(string); ok {
		ev.DeviceId = strings.ToUpper(ev.DeviceId)
	} else {
		log.Debug().Msg("Parser: device_id not found in field proc_id in message")
		return []*event.Event{ev}
	}

	ev.Type, ev.Info = extract(msgStr, p.ExprList)

	return p.postParseEvent(ev)
}

// обработки событий связанные с состоянием (предыдущими событиями) парсера
// Тут происходит сохранение и чтения из хранилища состояний
func (p *Parser) postParseEvent(ev *event.Event) []*event.Event {
	switch ev.Type {
	case event.TypePowerOn:
		ev.Timestamp = time.Now()
	case event.TypeHandsetCall, event.TypeHandsetStart:
		if pid, ok := ev.Info["pid"]; ok {
			pidStr := pid.(string)
			_ = p.StateDb.Set(fmt.Sprintf("%s_%s", ev.DeviceId, pid.(string)), pidStr, time.Minute)
		}
	case event.TypeHandsetCallError:
		if pid, ok := ev.Info["pid"]; ok && ev.DeviceId != "" {
			_, err := p.StateDb.Get(fmt.Sprintf("%s_%s", ev.DeviceId, pid.(string)))
			_ = p.StateDb.Del(fmt.Sprintf("%s_%s", ev.DeviceId, pid.(string)))
			if err != nil {
				handsetCall := ev.NewSimilarityWithType(event.TypeHandsetCall)
				return []*event.Event{handsetCall, ev}
			}
		}
	case event.TypeRfidFailed:
		// TODO: разобраться кому это нужно? В парсере не используется
		if rfid, ok := ev.Info["rfid_id"]; ok {
			_ = p.StateDb.Set(rfid.(string), rfid.(string), 5*time.Minute)
		}
	case event.TypeSipCall:
		if pid, ok := ev.Info["pid"]; ok {
			pidStr := pid.(string)
			if sip, ok := ev.Info["sip_account"]; ok {
				log.Debug().Msgf("Save sip call to statedb by pid %s", pidStr)
				_ = p.StateDb.Set(pidStr, sip.(string), 5*time.Minute)
				return []*event.Event{}
			} else {
				sip, err := p.StateDb.Get(pidStr)
				if err != nil {
					if err == redis.Nil {
						log.Debug().Msgf("Not found sip call from statedb by pid %s", pidStr)
					} else {
						log.Warn().Err(err).Msgf("Failed to get sip call from statedb by pid %s.", pidStr)
					}

				}
				_ = p.StateDb.Del(pidStr)
				ev.Info["sip_account"] = sip
			}
		}
	}
	return []*event.Event{ev}
}

func extract(text string, exprs []Expr) (evType string, evData map[string]interface{}) {
	var ok bool
	evData = map[string]interface{}{}
	evType = event.TypeUnknown

	for _, expr := range exprs {
		if expr.Str != "" {
			if strings.Contains(text, expr.Str) {
				if len(expr.SubExpr) > 0 {
					evType, evData = extract(text, expr.SubExpr)
				} else {
					if expr.TypeFunc == nil {
						evType = expr.Type
					} else {
						evType = expr.TypeFunc(evData)
					}

					if expr.AddInfo != nil {
						for k, v := range expr.AddInfo {
							evData[k] = v
						}
					}
				}
				return
			}
		} else if expr.Re != nil {
			if evType, evData, ok = extractByRegexp(text, &expr); ok {
				return
			}
		}
	}

	return
}

func extractByRegexp(text string, expr *Expr) (evType string, evData map[string]interface{}, ok bool) {
	evData = map[string]interface{}{}
	evType = event.TypeUnknown
	ok = false

	if reMatch := expr.Re.FindStringSubmatch(text); len(reMatch) != 0 {
		ok = true
		for i, name := range expr.Re.SubexpNames() {
			if name != "" && reMatch[i] != "" {
				evData[name] = reMatch[i]
			}
		}

		if expr.TypeFunc == nil {
			evType = expr.Type
		} else {
			evType = expr.TypeFunc(evData)
		}

		if expr.AddInfo != nil {
			for k, v := range expr.AddInfo {
				evData[k] = v
			}
		}
	}

	return
}
