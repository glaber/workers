// Copyright 2012 The GoSNMP Authors. All rights reserved.  Use of this
// source code is governed by a BSD-style license that can be found in the
// LICENSE file.

package main

import (
	"fmt"
	"time"
	"bufio"
	"os"
	"log"
	"encoding/json"	
	//"gosnmp"
	"strconv"
	//"net"
	"flag"
	 "github.com/gosnmp/gosnmp"
	//"encoding/binary"
)
/*
use this for checking
{"id":1, "ip":"127.0.0.1", "oid":".1.3", "type":"walk", "community":"public", "version":2, "port":161 }
{"id":1, "ip":"127.0.0.1", "oid":".1.3.6.1.2.1.28.1.1.2.1", "type":"get", "community":"public", "version":2, "port":161 }
{"id":1, "ip":"127.0.0.1", "oid":".1.3.6.1.2.1.28.1.1.2.1", "type":"walk", "community":"public", "version":2, "port":161 }

{"id":1, "ip":"8.8.8.8", "oid":".1.2.3.4", "type":"get", "community":"public", "version":2, "port":161 }
{"id":1, "ip":"8.8.8.8", "oid":".1.2.3.4", "type":"get", "community":"public", "version":2, "port":161 }
{"id":1, "ip":"8.8.8.8", "oid":".1.2.3.4", "type":"get", "community":"public", "version":2, "port":161 }
{"id":1, "ip":"8.8.8.8", "oid":".1.2.3.4", "type":"get", "community":"public", "version":2, "port":161 }
{"id":1, "ip":"8.8.8.8", "oid":".1.2.3.4", "type":"get", "community":"public", "version":2, "port":161 }
{"id":1, "ip":"8.8.8.8", "oid":".1.2.3.4", "type":"get", "community":"public", "version":2, "port":161 }
{"id":1, "ip":"1.1.1.3", "oid":".1.2.3.4", "type":"get", "community":"public", "version":2, "port":161 }
*/


const maxInBufferSize = 1024
const maxOutBufferSize = 1000000
//const pollerBufferSize = 16384

//const maxProcs	= 8192
//const concurency = 4


//const channels = maxProcs/concurency

const defaultTimeout = 5

type snmp_request_t struct {
	Ip 			string	`json:"ip"`
	Port 		uint16	`json:"port"`
	Oid 		string	`json:"oid"`
	Community 	string	`json:"community"`
	Version 	int		`json:"version"`
	Id			uint64	`json:"id"`
	Timeout 	int		`json:"timeout"`
	Type		string	`json:"type"`
	SecurityLevel 	string	`json:"security_level"`
	Username 	string	`json:"security_name"`
	ContextName string	`json:"context"`
	AuthProto 	string	`json:"auth_proto"`
	AuthPass 	string	`json:"auth_pass"`
	PrivProto 	string	`json:"priv_proto"`
	PrivPass 	string	`json:"priv_pass"`
}

type snmp_value_t struct {
	Oid 	string	`json:"oid"`
	Value	string	`json:"value"`
}

type response_get_t struct {
	Id 		uint64	`json:"id"`
	Code	int		`json:"code"`
	Value	string	`json:"value"`
}

type response_walk_t struct {
	Id 		uint64  `json:"id"`
	Code	int	 	`json:"code"`
	Values	[]snmp_value_t `json:"values"`
}

type response_error_t struct {
	Id 		uint64  `json:"id"`
	Code	int	 	`json:"code"`
	Error	string 	`json:"error"`
}

func main() {

	concurency := flag.Int("connects", 4, "Connections to use per one host")
	enable_debug:= flag.Int("debug", 0, "Set to non-zero to enable packet debugging")
	//reopen_socket:= flag.Int("reopen_socket", 0, "Open new socket for each new connection")
	source_ip:= flag.String("S", "", "Source IP address")


	flag.Parse()

	results := make(chan string, maxOutBufferSize)
	go std_writer(results)

	req_map := make(map[string](chan snmp_request_t))

	scanner := bufio.NewScanner(os.Stdin)
	request := snmp_request_t{Timeout:defaultTimeout}

	for scanner.Scan() {
		err := json.Unmarshal(scanner.Bytes(), &request)
		
		if err != nil {
			fmt.Println(err)
			continue
		}

		if (nil == req_map[request.Ip]) {
			req_map[request.Ip] = make( chan snmp_request_t, maxInBufferSize)
					
			for  i := 0; i < *concurency; i++ {
				go snmp_request_processor(request.Ip, request.Port, req_map[request.Ip], results, 
					*enable_debug, *source_ip)
			}
		}
		req_map[request.Ip] <- request
	}

	if err := scanner.Err(); err != nil {
		fmt.Println(err)
	}
}

func std_writer(responses <-chan string) {

	for result := range responses {
		fmt.Printf("%s\n",result)
	}
}

func snmp_request_processor(ip_addr string, port uint16, requests <-chan snmp_request_t, responses chan<- string, 
			enable_debug int, source_ip string) {
	var err error
	//var localAddr net.Addr
	//var net_conn net.Conn

	//if localAddr, err = net.ResolveUDPAddr("udp", source_ip); err != nil {
	//	return 
	//}
	for req := range requests {
		//net_conn, err = net.ListenUDP("udp", localAddr.(*net.UDPAddr))
	
	 	//dialer := net.Dialer{LocalAddr: localAddr}
	 	//conn_addr := fmt.Sprintf("%s:%d", ip_addr, port)
	 	//net_conn, err = dialer.Dial("udp", conn_addr );
	
	 	if (nil != err) {
	 		fmt.Println(err.Error())
	 		return
	 	}

		process_snmp_request(&req, responses, enable_debug, ip_addr, port)
	//	net_conn.
	//	net_conn.Close()
	}

//	net_conn.Close()
}

func respondError(id uint64, errcode int,  err_str string, responses chan<- string)  {
	var resp response_error_t

 	resp.Code = errcode
 	resp.Error = err_str
 	resp.Id = id

	str, err := json.Marshal(resp)

	if (nil != err) {
		fmt.Println(err.Error())
		return
	}

	responses <- string(str)
 }



func respondWalkResultValue(id uint64, results []gosnmp.SnmpPDU, responses chan<- string) {
	var resp response_walk_t
	
	resp.Id = id
	resp.Code = 200
	
	resp.Values = make([]snmp_value_t, len(results))

	for i, pdu := range results {
		resp.Values[i].Oid = pdu.Name

		switch pdu.Type {
		
		case gosnmp.IPAddress, gosnmp.ObjectIdentifier, gosnmp.ObjectDescription, gosnmp.NsapAddress:
			resp.Values[i].Value =  pdu.Value.(string);
		
		case gosnmp.OctetString, gosnmp.BitString:
			resp.Values[i].Value = string(pdu.Value.([]byte))
		
		case gosnmp.Boolean, gosnmp.Integer, gosnmp.Counter32, gosnmp.Gauge32, gosnmp.Counter64, gosnmp.Uinteger32, gosnmp.TimeTicks:
			resp.Values[i].Value = gosnmp.ToBigInt(pdu.Value).String()
		
		case gosnmp.OpaqueFloat:
			resp.Values[i].Value = strconv.FormatFloat(float64(pdu.Value.(float32)), 'f', 6, 64)
			
		case gosnmp.OpaqueDouble:
			resp.Values[i].Value = strconv.FormatFloat(pdu.Value.(float64), 'f', 6, 64)
			
		case gosnmp.Null:
			resp.Values[i].Value = "Null"
		
		case gosnmp.UnknownType:
			resp.Values[i].Value = "Unknown PDU type"
		
		default:
			//fmt.Printf("Got unknow pdu type %d, value %s \n", pdu.Type, pdu.Value)
			resp.Values[i].Value = gosnmp.ToBigInt(pdu.Value).String()
	   }
	}

	str, err := json.Marshal(resp)

	if (nil != err) {
		fmt.Println(err.Error())
		return
	}

	responses <- string(str)
}


func respondGetResult(id uint64, result *gosnmp.SnmpPacket, responses chan<- string, req_oid *string ) {
	var resp response_get_t	
	
	resp.Id = id
	resp.Code = 200
	
	
	for _, pdu := range result.Variables {
		//fmt.Printf("Got pdu type %d, \n", pdu.Type);
		switch pdu.Type {
		
		case gosnmp.IPAddress, gosnmp.ObjectIdentifier, gosnmp.ObjectDescription, gosnmp.NsapAddress:
			resp.Value =  pdu.Value.(string);
		
		case gosnmp.OctetString, gosnmp.BitString:
			resp.Value = string(pdu.Value.([]byte))
		
		case gosnmp.Boolean, gosnmp.Integer, gosnmp.Counter32, gosnmp.Gauge32, gosnmp.Counter64, gosnmp.Uinteger32, gosnmp.TimeTicks:
			resp.Value = gosnmp.ToBigInt(pdu.Value).String()
		
		case gosnmp.OpaqueFloat:
			resp.Value = strconv.FormatFloat(float64(pdu.Value.(float32)), 'f', 6, 64)
			
		case gosnmp.OpaqueDouble:
			resp.Value = strconv.FormatFloat(pdu.Value.(float64), 'f', 6, 64)
			
		case gosnmp.Null:
			resp.Value = "Null"
		
		case gosnmp.UnknownType:
			resp.Value = "Unknown PDU type"
		
		case gosnmp.NoSuchInstance:
			respondError(id, 503, "No such OID, check configuration", responses)
			return
		
		case gosnmp.NoSuchObject:
			respondError(id, 503, "No such object, check configuration", responses)
			return
		
		default:
		//	fmt.Printf("Got unknow pdu type %d, value %s \n", pdu.Type, pdu.Value)
			resp.Value = gosnmp.ToBigInt(pdu.Value).String()
	   }
	
	   str, err := json.Marshal(resp)

	 	if (nil != err) {
	 		fmt.Println(err.Error())
	 		return
	 	}

	 	responses <- string(str)
	}



	// for _, variable := range result.Variables {
		
	// 	switch variable.Type {
	//  	case gosnmp.OctetString:
	//  		resp.Value = string(variable.Value.([]byte))

	// 	case gosnmp.ObjectIdentifier:	
	// 		resp.Value =  variable.Value.(string);
	// 	case gosnmp.IPAddress:
	// 		resp.Value =  variable.Value.(string);
	//  	default:
	// 		resp.Value = string(variable.Value.([]byte))
	//  		//resp.Value = gosnmp.ToBigInt(variable.Value).String()
	// 	}

	// 	
	// }
}

func process_snmp_request(req *snmp_request_t, responses chan<- string, 
		enable_debug int, ip_addr string, port uint16) {
	
	var sec_params gosnmp.UsmSecurityParameters
	var conn gosnmp.GoSNMP
	
	
	if (enable_debug > 0) {
		conn.Logger = gosnmp.NewLogger(log.New(os.Stdout, "", 0))
	}

	conn.Target = req.Ip
	conn.Port = req.Port
	conn.Community = req.Community
	conn.Timeout = time.Duration(req.Timeout) * time.Second
	conn.MaxOids = 5
	conn.UseUnconnectedUDPSocket  = false
	//conn.UseUnconnectedUDPSocket  = true
	// if ( 0 < reopen_socket) {
	//conn.Conn = net_conn
	// }
	conn.Retries = 2
	// //conn.PrepareConnect(reopen_socket)
	conn.Connect()
	//if err != nil {
	 //	log.Fatalf("Connect() err: %v", err)
	// }
	defer conn.Conn.Close()

	switch (req.Version) {
	case 1:
		conn.Version = gosnmp.Version1
	case 2:
		conn.Version = gosnmp.Version2c
	case 3: 
		conn.Version = gosnmp.Version3
	}

	if (3 == req.Version) {
		conn.SecurityModel = gosnmp.UserSecurityModel
		sec_params.UserName = req.Username

		switch (req.SecurityLevel) {
		case "no":
			conn.MsgFlags = gosnmp.NoAuthNoPriv
		case "authnopriv":
			conn.MsgFlags = gosnmp.AuthNoPriv
		case "authpriv":
			conn.MsgFlags =gosnmp.AuthPriv
		}

		conn.ContextName = req.ContextName
	
		if (gosnmp.AuthNoPriv == conn.MsgFlags || 
			gosnmp.AuthPriv == conn.MsgFlags ) {
	
			switch (req.AuthProto) {
			case "MD5":
				sec_params.AuthenticationProtocol = gosnmp.MD5
			case "SHA1":
				sec_params.AuthenticationProtocol = gosnmp.SHA
			case "SHA224":
				sec_params.AuthenticationProtocol = gosnmp.SHA224
			case "SHA256":
				sec_params.AuthenticationProtocol = gosnmp.SHA256
			case "SHA384":
				sec_params.AuthenticationProtocol = gosnmp.SHA384
			case "SHA512":
				sec_params.AuthenticationProtocol = gosnmp.SHA512
			}	
		
			sec_params.AuthenticationPassphrase = req.AuthPass
		}
		
		if (gosnmp.AuthPriv == conn.MsgFlags) {
			switch (req.PrivProto) {
			case "DES":
				sec_params.PrivacyProtocol = gosnmp.DES
			case "AES128":
				sec_params.PrivacyProtocol = gosnmp.AES
			case "AES192":
				sec_params.PrivacyProtocol = gosnmp.AES192
			case "AES256":
				sec_params.PrivacyProtocol = gosnmp.AES256
			case "AES192C":
				sec_params.PrivacyProtocol = gosnmp.AES192C
			case "AES256C":
				sec_params.PrivacyProtocol = gosnmp.AES256C
			}

			sec_params.PrivacyPassphrase = req.PrivPass
		}
		conn.SecurityParameters = &sec_params
	}
	// if (0 == reopen_socket) {
	// 	err := conn.UpdateAddress()
	
	// 	if err != nil {
	// 		respondError(req.Id, 503, err.Error(), responses)
	// 		return
	// 	}
	// }
	
	if (req.Type == "walk") {

		results, err := conn.WalkAll(req.Oid)
		
		if (nil == err) {
			respondWalkResultValue(req.Id, results, responses)
		} else  {
			respondError(req.Id, 408, err.Error(), responses)
		}  

	} else {
		oids := []string{req.Oid}
		result, err := conn.Get(oids);

		if (nil == err ) {
			respondGetResult(req.Id, result, responses, &req.Oid)
		} else {
			respondError(req.Id, 408, err.Error(), responses)
		}
	}
}