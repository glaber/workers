Name:		glaber-workers
Version:	2.0.7
Release:	%{?alphatag:0.}1%{?alphatag}%{?dist}
Summary:	The Enterprise-class open source monitoring solution
Group:		Applications/Internet
License:	GPLv2+
URL:		http://glaber.io
Source0:	%{name}-%{version}%{?alphatag}.tar.gz

Buildroot:	%{_tmppath}/glaber-workers-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	make

%description
Glaber is a fork of Zabbix with efficiency and speed improvements.
Zabbix is the ultimate enterprise-level software designed for
real-time monitoring of millions of metrics collected from tens of
thousands of servers, virtual machines and network devices.

%package glaber-workers
Summary:		Glaber workers for additional glaber functionality
Group:			Applications/Internet
BuildArch:		noarch

%description glaber-workers
Glaber workers for additional glaber functionality

#
# prep
#

%files
/usr/sbin/glb_hist_victoria
/usr/sbin/glb_netflow_collector
#/usr/sbin/glb_ripe_subscriber
/usr/sbin/glb_syslog_sender
/usr/sbin/glb_syslog_worker
/usr/sbin/glb_snmp_worker
/usr/sbin/glb_snmp_worker_reopen_socket
#/usr/sbin/glb_snmptrap_worker
#/usr/sbin/glb_snmptrap_worker2


%prep
%setup
pwd
ls -al

#%build
#fugire where it builds

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/usr/sbin
install -m 0755 -p glb_* $RPM_BUILD_ROOT%{_sbindir}/

%clean
rm -rf $RPM_BUILD_ROOT